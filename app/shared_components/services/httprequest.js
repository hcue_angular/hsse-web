hsscApp.run(function ($http) {
		//$http.defaults.headers.common.post={};
		$http.defaults.headers.post={'content-type':'application/x-www-form-urlencoded'}
        //$http.defaults.headers.post={'content-type':'multipart/form-data'}
	});
hsscApp.factory('httpInterceptor', function ($q, $rootScope, $log) {
        var numLoadings = 0;

        return {
            request: function (config) {
                
                numLoadings++;
                $('#app-loader').addClass('show');
                return config || $q.when(config)

            },
            response: function (response) {
                if (response.config.loaderStatus == undefined) {
                    if ((--numLoadings) === 0) {
                        // Hide loader
                        $('#app-loader').removeClass('show');
                    }
                }
                return response || $q.when(response);

            },
            responseError: function (response) {
                if (response.config.loaderStatus == undefined) {
                    if (!(--numLoadings)) {
                        // Hide loader
                        $('#app-loader').removeClass('show');
                    }
                }
                return $q.reject(response);
            }
        };
    });

hsscApp.service('httpRequestService', function ($http) {
        this.connect=function(url,method,data){
            if(method=="POST"){
                return $http.post(url,data);
            }
            else{
                return $http.get(url);
            }
        }
    });

hsscApp.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
     }]);


