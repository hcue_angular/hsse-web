hsscApp.factory('appLinkFactory', function (CONFIG) {
        var url={};
        url.baseUrl={
            "dev": "https://aadev.accentureanalytics.com/hsseapi/api/CmsApi",
        	"staging":"https://switchtest.gametize.com/api",
        	"prod":"https://switch.petronas.com/api"
        }
        url.endPoint={
            "getGlobalCaseValue": "/Get_uauc_rpt_ytd_data",
            "getCasesOpenValue": "/Get_uauc_rpt_ytd_data",
            "Getquadrantplotdata": "/Get_quadrant_plot_data",
            "GetOPULevelPercentage" : "/Get_correlation_score_dulang_percentage",
            "getDulangProbability": "/Get_dulang_uauc_score_count",
            "Getcorrelationscoredulangdata": "/Get_correlation_score_dulang_data",
            "getdulanguaucscoredata": "/Get_dulang_uauc_score_data",
            "GetDulangUAUCCatPercentage": "/Get_uauc_categories_score_percentage_data",
            "GetSiteLevelPie": "/Get_correlation_score_dulang_correlation_percentage",
            "Getcorrelationscorepercentagepotentialriskwise": "/Get_correlation_score_percentage_potential_risk_wise",
            "GetDulangPie": "/Get_correlation_score_percentage_potential_risk_wise",
            "GetDulangPieDrillDown": "/Get_correlation_score_dulang_correlation_percentage",
            "GetIncidentWiseCorrelation": "/Get_incident_wise_correlation_percentage",
            "getChord": "/Get_uauc_score_element_count_subcount",
            "getElementTable": "/Get_uauc_score_element_count",
            "getDulangValue": "/Get_dulang_count",
            "getLessonLearntValue": "/Get_dulang_uauc_score_select"
          
        }
        url.getUrl=function(module){
        	return url.baseUrl[CONFIG.environment] + url.endPoint[module];
        }
        return url;
    });
