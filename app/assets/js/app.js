'use strict';

var hsscApp = angular.module('hssc', [
  'ngRoute',
  'hssc.dashboard',
  'ngCookies',
  'ngAlertify'
])

.constant('CONFIG', {
	environment:"dev", //  "dev"/"staging"/"prod"
})
.run(['$location','$rootScope', function($location,$rootScope) {
		$location.path('/dashboard');
}])



