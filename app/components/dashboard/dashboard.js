
angular.module('hssc.dashboard', ['ngRoute', 'ngAlertify'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/dashboard', {
            templateUrl: 'app/components/dashboard/dashboard.html',
            controller: 'dashboardController'
        });
    }])

    .controller('dashboardController', ['$scope', '$location', 'CONFIG', 'alertify', 'appLinkFactory', 'httpRequestService', 'appCookieFactory',
    function ($scope, $location, CONFIG, alertify, appLinkFactory, httpRequestService, appCookieFactory) {
        $scope.dataCollections = {

            "name": "pm",
            "high": 94,
            "medium": 92,
            "low": 74,
            "casereported": {
                "ua": 158009,
                "uc": 52699
            },
            "caseclosed": {
                "ua": 83386,
                "uc": 27795
            },
            "caseopened": {
                "ua": 74712,
                "uc": 24904
            },
            "alert": {
                "lifting": 120,
                "welding": 40,
                "wellhead": 30
            },

            "groups": [
                {
                "name": "dulang",
                "high": 65,
                "medium": 66,
                "low": 33,
                "casereported": {
                    "ua": 118009,
                    "uc": 32699
                },
                "caseclosed": {
                    "ua": 43386,
                    "uc": 17795
                },
                "caseopened": {
                    "ua": 44712,
                    "uc": 14904
                },
                "alert": {
                    "lifting": 120,
                    "welding": 40,
                    "wellhead": 30
                },
                "groups": [{
                    "name": "dulang",
                    "high": 51,
                    "medium": 55,
                    "low": 22,
                    "casereported": {
                        "ua": 108009,
                        "uc": 31699
                    },
                    "caseclosed": {
                        "ua": 42386,
                        "uc": 14795
                    },
                    "caseopened": {
                        "ua": 24712,
                        "uc": 4904
                    },
                    "alert": {
                        "lifting": 120,
                        "welding": 40,
                        "wellhead": 30
                    },
                    "groups": [{
                        "name": "pm",
                        "high": 36,
                        "medium": 34,
                        "low": 45,
                        "casereported": {
                            "ua": 18009,
                            "uc": 21699
                        },
                        "caseclosed": {
                            "ua": 22386,
                            "uc": 4795
                        },
                        "caseopened": {
                            "ua": 21712,
                            "uc": 3904
                        },
                        "alert": {
                            "lifting": 120,
                            "welding": 40,
                            "wellhead": 30
                        },

                        "groups": [

                            {
                                "name": "dulang",
                                "high": 10,
                                "medium": 13,
                                "low": 5,
                                "casereported": {
                                    "ua": 12009,
                                    "uc": 11699
                                },
                                "caseclosed": {
                                    "ua": 12386,
                                    "uc": 2795
                                },
                                "caseopened": {
                                    "ua": 11712,
                                    "uc": 2904
                                },
                                "alert": {
                                    "lifting": 120,
                                    "welding": 40,
                                    "wellhead": 30
                                },
                                "groups": [{
                                    "name": "dulang",
                                    "high": 10,
                                    "medium": 13,
                                    "low": 5,
                                    "casereported": {
                                        "ua": 12009,
                                        "uc": 11699
                                    },
                                    "caseclosed": {
                                        "ua": 12386,
                                        "uc": 2795
                                    },
                                    "caseopened": {
                                        "ua": 11712,
                                        "uc": 2904
                                    },
                                    "alert": {
                                        "lifting": 120,
                                        "welding": 40,
                                        "wellhead": 30
                                    },
                                }],
                            },
                            {
                                "name": "Bekok",
                                "high": 4,
                                "medium": 5,
                                "low": 6,
                                "casereported": {
                                    "ua": 31200,
                                    "uc": 21012
                                },
                                "caseclosed": {
                                    "ua": 21340,
                                    "uc": 23000
                                },
                                "caseopened": {
                                    "ua": 43560,
                                    "uc": 34500
                                },
                            },
                            {
                                "name": "tiong",
                                "high": 3,
                                "medium": 5,
                                "low": 8,
                                "casereported": {
                                    "ua": 3200,
                                    "uc": 34000
                                },
                                "caseclosed": {
                                    "ua": 23400,
                                    "uc": 40122
                                },
                                "caseopened": {
                                    "ua": 23300,
                                    "uc": 37500
                                },
                            },
                            {
                                "name": "osc",
                                "high": 3,
                                "medium": 6,
                                "low": 9,
                                "casereported": {
                                    "ua": 32101,
                                    "uc": 37500
                                },
                                "caseclosed": {
                                    "ua": 51002,
                                    "uc": 35012
                                },
                                "caseopened": {
                                    "ua": 11010,
                                    "uc": 2300
                                },
                            },
                            {
                                "name": "tcot",
                                "high": 10,
                                "medium": 9,
                                "low": 10,
                                "casereported": {
                                    "ua": 9100,
                                    "uc": 3500
                                },
                                "caseclosed": {
                                    "ua": 72100,
                                    "uc": 35003
                                },
                                "caseopened": {
                                    "ua": 41200,
                                    "uc": 20200
                                },
                            },
                            {
                                "name": "angsi",
                                "high": 5,
                                "medium": 6,
                                "low": 7,
                                "casereported": {
                                    "ua": 35112,
                                    "uc": 43302
                                },
                                "caseclosed": {
                                    "ua": 65700,
                                    "uc": 20900
                                },
                                "caseopened": {
                                    "ua": 44500,
                                    "uc": 10110
                                },
                            }
                        ]

                    },
                        {
                            "name": "skg",
                            "high": 8,
                            "medium": 10,
                            "low": 8,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        },
                        {
                            "name": "sko",
                            "high": 4,
                            "medium": 6,
                            "low": 10,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        },
                        {
                            "name": "sb",
                            "high": 4,
                            "medium": 5,
                            "low": 4,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        }
                    ]
                },
                    {
                        "name": "ia",
                        "high": 8,
                        "medium": 7,
                        "low": 4,
                        "casereported": {
                            "ua": 34000,
                            "uc": 34900
                        },
                        "caseclosed": {
                            "ua": 54000,
                            "uc": 34567
                        },
                        "caseopened": {
                            "ua": 13200,
                            "uc": 34500
                        },
                    },
                    {
                        "name": "lng",
                        "high": 5,
                        "medium": 4,
                        "low": 7,
                        "casereported": {
                            "ua": 34000,
                            "uc": 55600
                        },
                        "caseclosed": {
                            "ua": 34570,
                            "uc": 45677
                        },
                        "caseopened": {
                            "ua": 19019,
                            "uc": 45670
                        },
                    }

                ]
            },
                {
                    "name": "downstream",
                    "high": 10,
                    "medium": 4,
                    "low": 4,
                    "casereported": {
                        "ua": 34500,
                        "uc": 36890
                    },
                    "caseclosed": {
                        "ua": 12345,
                        "uc": 54312
                    },
                    "caseopened": {
                        "ua": 97056,
                        "uc": 17986
                    },
                },
                {
                    "name": "pd&t",
                    "high": 2,
                    "medium": 3,
                    "low": 10,
                    "casereported": {
                        "ua": 34760,
                        "uc": 97650
                    },
                    "caseclosed": {
                        "ua": 43000,
                        "uc": 23467
                    },
                    "caseopened": {
                        "ua": 78950,
                        "uc": 93120
                    },
                },
                {
                    "name": "misc",
                    "high": 10,
                    "medium": 4,
                    "low": 10,
                    "casereported": {
                        "ua": 32900,
                        "uc": 45670
                    },
                    "caseclosed": {
                        "ua": 12657,
                        "uc": 89600
                    },
                    "caseopened": {
                        "ua": 32567,
                        "uc": 98650
                    },
                },
                {
                    "name": "klcch",
                    "high": 3,
                    "medium": 5,
                    "low": 10,
                    "casereported": {
                        "ua": 14301,
                        "uc": 43678
                    },
                    "caseclosed": {
                        "ua": 34678,
                        "uc": 43567
                    },
                    "caseopened": {
                        "ua": 36578,
                        "uc": 76890
                    },
                },
                {
                    "name": "others",
                    "high": 4,
                    "medium": 10,
                    "low": 7,
                    "casereported": {
                        "ua": 12789,
                        "uc": 98765
                    },
                    "caseclosed": {
                        "ua": 9065,
                        "uc": 6789
                    },
                    "caseopened": {
                        "ua": 4387,
                        "uc": 9845
                    },
                }
            ]
        };


        $scope.dataCollections1 = {

            "name": "apac",
            "high": 74,
            "medium": 52,
            "low": 34,
            "casereported": {
                "ua": 118009,
                "uc": 22699
            },
            "caseclosed": {
                "ua": 43386,
                "uc": 17795
            },
            "caseopened": {
                "ua": 54712,
                "uc": 14904
            },
            "alert": {
                "lifting": 46,
                "welding": 15,
                "wellhead": 1
            },

            "groups": [{
                "name": "upstream",
                "high": 35,
                "medium": 36,
                "low": 13,
                "casereported": {
                    "ua": 110009,
                    "uc": 12699
                },
                "caseclosed": {
                    "ua": 23386,
                    "uc": 7795
                },
                "caseopened": {
                    "ua": 53712,
                    "uc": 11904
                },
                "alert": {
                    "lifting": 4,
                    "welding": 54,
                    "wellhead": 67
                },
                "groups": [{
                    "name": "ma",
                    "high": 32,
                    "medium": 35,
                    "low": 12,
                    "casereported": {
                        "ua": 11000,
                        "uc": 11599
                    },
                    "caseclosed": {
                        "ua": 11386,
                        "uc": 3795
                    },
                    "caseopened": {
                        "ua": 23712,
                        "uc": 1904
                    },
                    "alert": {
                        "lifting": 120,
                        "welding": 40,
                        "wellhead": 40
                    },
                    "groups": [{
                        "name": "pm",
                        "high": 26,
                        "medium": 24,
                        "low": 25,
                        "casereported": {
                            "ua": 10000,
                            "uc": 9599
                        },
                        "caseclosed": {
                            "ua": 1386,
                            "uc": 1795
                        },
                        "caseopened": {
                            "ua": 21712,
                            "uc": 1504
                        },
                        "alert": {
                            "lifting": 146,
                            "welding": 125,
                            "wellhead": 14
                        },

                        "groups": [{
                            "name": "dulang",
                            "high": 7,
                            "medium": 2,
                            "low": 1,
                            "casereported": {
                                "ua": 9000,
                                "uc": 7199
                            },
                            "caseclosed": {
                                "ua": 1286,
                                "uc": 1395
                            },
                            "caseopened": {
                                "ua": 2712,
                                "uc": 504
                            },
                            "alert": {
                                "lifting": 120,
                                "welding": 40,
                                "wellhead": 30
                            },
                            "groups": [{
                                "name": "dulang",
                                "high": 10,
                                "medium": 13,
                                "low": 5,
                                "casereported": {
                                    "ua": 5000,
                                    "uc": 3199
                                },
                                "caseclosed": {
                                    "ua": 1186,
                                    "uc": 1195
                                },
                                "caseopened": {
                                    "ua": 2412,
                                    "uc": 2504
                                },
                                "alert": {
                                    "lifting": 120,
                                    "welding": 40,
                                    "wellhead": 30
                                },
                            }],
                        },
                            {
                                "name": "Bekok",
                                "high": 1,
                                "medium": 3,
                                "low": 5,
                                "casereported": {
                                    "ua": 3120,
                                    "uc": 2012
                                },
                                "caseclosed": {
                                    "ua": 11310,
                                    "uc": 11000
                                },
                                "caseopened": {
                                    "ua": 23510,
                                    "uc": 14100
                                },
                            },
                            {
                                "name": "tiong",
                                "high": 2,
                                "medium": 1,
                                "low": 5,
                                "casereported": {
                                    "ua": 3200,
                                    "uc": 32000
                                },
                                "caseclosed": {
                                    "ua": 21400,
                                    "uc": 4122
                                },
                                "caseopened": {
                                    "ua": 21300,
                                    "uc": 3500
                                },
                            },
                            {
                                "name": "osc",
                                "high": 2,
                                "medium": 4,
                                "low": 5,
                                "casereported": {
                                    "ua": 12111,
                                    "uc": 2750
                                },
                                "caseclosed": {
                                    "ua": 21002,
                                    "uc": 15012
                                },
                                "caseopened": {
                                    "ua": 21010,
                                    "uc": 1300
                                },
                            },
                            {
                                "name": "tcot",
                                "high": 5,
                                "medium": 3,
                                "low": 1,
                                "casereported": {
                                    "ua": 7100,
                                    "uc": 2500
                                },
                                "caseclosed": {
                                    "ua": 52100,
                                    "uc": 65003
                                },
                                "caseopened": {
                                    "ua": 2100,
                                    "uc": 13200
                                },
                            },
                            {
                                "name": "angsi",
                                "high": 2,
                                "medium": 3,
                                "low": 4,
                                "casereported": {
                                    "ua": 1512,
                                    "uc": 23402
                                },
                                "caseclosed": {
                                    "ua": 35700,
                                    "uc": 10900
                                },
                                "caseopened": {
                                    "ua": 22500,
                                    "uc": 110
                                },
                            }
                        ]

                    },
                        {
                            "name": "skg",
                            "high": 3,
                            "medium": 1,
                            "low": 4,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        },
                        {
                            "name": "sko",
                            "high": 2,
                            "medium": 3,
                            "low": 5,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        },
                        {
                            "name": "sb",
                            "high": 1,
                            "medium": 3,
                            "low": 2,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        }
                    ]
                },
                    {
                        "name": "ia",
                        "high": 4,
                        "medium": 3,
                        "low": 2,
                        "casereported": {
                            "ua": 20000,
                            "uc": 12900
                        },
                        "caseclosed": {
                            "ua": 32000,
                            "uc": 30567
                        },
                        "caseopened": {
                            "ua": 11200,
                            "uc": 21500
                        },
                    },
                    {
                        "name": "lng",
                        "high": 2,
                        "medium": 2,
                        "low": 4,
                        "casereported": {
                            "ua": 14000,
                            "uc": 35600
                        },
                        "caseclosed": {
                            "ua": 14570,
                            "uc": 25677
                        },
                        "caseopened": {
                            "ua": 919,
                            "uc": 2670
                        },
                    }

                ]
            },
                {
                    "name": "downstream",
                    "high": 5,
                    "medium": 2,
                    "low": 2,
                    "casereported": {
                        "ua": 24500,
                        "uc": 16890
                    },
                    "caseclosed": {
                        "ua": 2345,
                        "uc": 24312
                    },
                    "caseopened": {
                        "ua": 57056,
                        "uc": 986
                    },
                },
                {
                    "name": "pd&t",
                    "high": 1,
                    "medium": 2,
                    "low": 5,
                    "casereported": {
                        "ua": 24760,
                        "uc": 57650
                    },
                    "caseclosed": {
                        "ua": 13000,
                        "uc": 13467
                    },
                    "caseopened": {
                        "ua": 38950,
                        "uc": 83120
                    },
                },
                {
                    "name": "misc",
                    "high": 5,
                    "medium": 2,
                    "low": 5,
                    "casereported": {
                        "ua": 12900,
                        "uc": 2670
                    },
                    "caseclosed": {
                        "ua": 2557,
                        "uc": 42600
                    },
                    "caseopened": {
                        "ua": 11567,
                        "uc": 54650
                    },
                },
                {
                    "name": "klcch",
                    "high": 2,
                    "medium": 3,
                    "low": 5,
                    "casereported": {
                        "ua": 12301,
                        "uc": 21678
                    },
                    "caseclosed": {
                        "ua": 12378,
                        "uc": 22367
                    },
                    "caseopened": {
                        "ua": 16578,
                        "uc": 16890
                    },
                },
                {
                    "name": "others",
                    "high": 2,
                    "medium": 5,
                    "low": 3,
                    "casereported": {
                        "ua": 11489,
                        "uc": 54765
                    },
                    "caseclosed": {
                        "ua": 6500,
                        "uc": 6248
                    },
                    "caseopened": {
                        "ua": 2287,
                        "uc": 4545
                    },
                }
            ]
        };

        $scope.dataCollections2 = {

            "name": "malaysia",
            "high": 14,
            "medium": 12,
            "low": 10,
            "casereported": {
                "ua": 128009,
                "uc": 22699
            },
            "caseclosed": {
                "ua": 43386,
                "uc": 17795
            },
            "caseopened": {
                "ua": 44712,
                "uc": 14904
            },
            "alert": {
                "lifting": 6,
                "welding": 3,
                "wellhead": 5
            },

            "groups": [{
                "name": "upstream",
                "high": 5,
                "medium": 6,
                "low": 3,
                "casereported": {
                    "ua": 112009,
                    "uc": 12699
                },
                "caseclosed": {
                    "ua": 23386,
                    "uc": 7795
                },
                "caseopened": {
                    "ua": 12712,
                    "uc": 2904
                },
                "alert": {
                    "lifting": 1,
                    "welding": 3,
                    "wellhead": 10
                },

                "groups": [{
                    "name": "ma",
                    "high": 2,
                    "medium": 5,
                    "low": 2,
                    "casereported": {
                        "ua": 111009,
                        "uc": 11699
                    },
                    "caseclosed": {
                        "ua": 21286,
                        "uc": 4795
                    },
                    "caseopened": {
                        "ua": 11712,
                        "uc": 2704
                    },
                    "alert": {
                        "lifting": 10,
                        "welding": 3,
                        "wellhead": 3
                    },
                    "groups": [{
                        "name": "pm",
                        "high": 3,
                        "medium": 4,
                        "low": 5,
                        "casereported": {
                            "ua": 11009,
                            "uc": 1199
                        },
                        "caseclosed": {
                            "ua": 2286,
                            "uc": 2795
                        },
                        "caseopened": {
                            "ua": 1112,
                            "uc": 2204
                        },
                        "alert": {
                            "lifting": 11,
                            "welding": 30,
                            "wellhead": 0
                        },

                        "groups": [

                            {
                                "name": "dulang",
                                "high": 1,
                                "medium": 2,
                                "low": 1,
                                "casereported": {
                                    "ua": 109,
                                    "uc": 19
                                },
                                "caseclosed": {
                                    "ua": 2186,
                                    "uc": 2295
                                },
                                "caseopened": {
                                    "ua": 112,
                                    "uc": 204
                                },
                                "alert": {
                                    "lifting": 120,
                                    "welding": 40,
                                    "wellhead": 30
                                },
                                "groups": [{
                                    "name": "dulang",
                                    "high": 10,
                                    "medium": 13,
                                    "low": 5,
                                    "casereported": {
                                        "ua": 109,
                                        "uc": 19
                                    },
                                    "caseclosed": {
                                        "ua": 2186,
                                        "uc": 2295
                                    },
                                    "caseopened": {
                                        "ua": 112,
                                        "uc": 204
                                    },
                                    "alert": {
                                        "lifting": 120,
                                        "welding": 40,
                                        "wellhead": 30
                                    },
                                }],
                            },
                            {
                                "name": "Bekok",
                                "high": 2,
                                "medium": 1,
                                "low": 3,
                                "casereported": {
                                    "ua": 30,
                                    "uc": 22
                                },
                                "caseclosed": {
                                    "ua": 40,
                                    "uc": 12
                                },
                                "caseopened": {
                                    "ua": 43,
                                    "uc": 30
                                },
                            },
                            {
                                "name": "tiong",
                                "high": 1,
                                "medium": 2,
                                "low": 4,
                                "casereported": {
                                    "ua": 3200,
                                    "uc": 340
                                },
                                "caseclosed": {
                                    "ua": 200,
                                    "uc": 122
                                },
                                "caseopened": {
                                    "ua": 233,
                                    "uc": 500
                                },
                            },
                            {
                                "name": "osc",
                                "high": 2,
                                "medium": 3,
                                "low": 2,
                                "casereported": {
                                    "ua": 321,
                                    "uc": 37
                                },
                                "caseclosed": {
                                    "ua": 512,
                                    "uc": 312
                                },
                                "caseopened": {
                                    "ua": 111,
                                    "uc": 20
                                },
                            },
                            {
                                "name": "tcot",
                                "high": 1,
                                "medium": 3,
                                "low": 5,
                                "casereported": {
                                    "ua": 91,
                                    "uc": 30
                                },
                                "caseclosed": {
                                    "ua": 217,
                                    "uc": 353
                                },
                                "caseopened": {
                                    "ua": 410,
                                    "uc": 20
                                },
                            },
                            {
                                "name": "angsi",
                                "high": 2,
                                "medium": 3,
                                "low": 4,
                                "casereported": {
                                    "ua": 352,
                                    "uc": 302
                                },
                                "caseclosed": {
                                    "ua": 600,
                                    "uc": 20
                                },
                                "caseopened": {
                                    "ua": 500,
                                    "uc": 110
                                },
                            }
                        ]

                    },
                        {
                            "name": "skg",
                            "high": 4,
                            "medium": 3,
                            "low": 4,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        },
                        {
                            "name": "sko",
                            "high": 2,
                            "medium": 3,
                            "low": 5,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        },
                        {
                            "name": "sb",
                            "high": 2,
                            "medium": 3,
                            "low": 3,
                            "casereported": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseclosed": {
                                "ua": 0,
                                "uc": 0
                            },
                            "caseopened": {
                                "ua": 0,
                                "uc": 0
                            },
                        }
                    ]
                },
                    {
                        "name": "ia",
                        "high": 4,
                        "medium": 3,
                        "low": 2,
                        "casereported": {
                            "ua": 340,
                            "uc": 390
                        },
                        "caseclosed": {
                            "ua": 5940,
                            "uc": 347
                        },
                        "caseopened": {
                            "ua": 130,
                            "uc": 340
                        },
                    },
                    {
                        "name": "lng",
                        "high": 2,
                        "medium": 2,
                        "low": 4,
                        "casereported": {
                            "ua": 3400,
                            "uc": 560
                        },
                        "caseclosed": {
                            "ua": 3570,
                            "uc": 4567
                        },
                        "caseopened": {
                            "ua": 1919,
                            "uc": 467
                        },
                    }

                ]
            },
                {
                    "name": "downstream",
                    "high": 1,
                    "medium": 2,
                    "low": 2,
                    "casereported": {
                        "ua": 300,
                        "uc": 390
                    },
                    "caseclosed": {
                        "ua": 145,
                        "uc": 512
                    },
                    "caseopened": {
                        "ua": 976,
                        "uc": 186
                    },
                },
                {
                    "name": "pd&t",
                    "high": 1,
                    "medium": 2,
                    "low": 5,
                    "casereported": {
                        "ua": 340,
                        "uc": 970
                    },
                    "caseclosed": {
                        "ua": 431,
                        "uc": 267
                    },
                    "caseopened": {
                        "ua": 780,
                        "uc": 920
                    },
                },
                {
                    "name": "misc",
                    "high": 5,
                    "medium": 2,
                    "low": 5,
                    "casereported": {
                        "ua": 320,
                        "uc": 450
                    },
                    "caseclosed": {
                        "ua": 157,
                        "uc": 860
                    },
                    "caseopened": {
                        "ua": 327,
                        "uc": 860
                    },
                },
                {
                    "name": "klcch",
                    "high": 2,
                    "medium": 3,
                    "low": 5,
                    "casereported": {
                        "ua": 14,
                        "uc": 478
                    },
                    "caseclosed": {
                        "ua": 348,
                        "uc": 467
                    },
                    "caseopened": {
                        "ua": 368,
                        "uc": 76
                    },
                },
                {
                    "name": "others",
                    "high": 2,
                    "medium": 4,
                    "low": 3,
                    "casereported": {
                        "ua": 89,
                        "uc": 65
                    },
                    "caseclosed": {
                        "ua": 65,
                        "uc": 89
                    },
                    "caseopened": {
                        "ua": 87,
                        "uc": 5
                    },
                }
            ]
        };


        $scope.chrodCollection = {
            "collection1": [
                {
                    "sourceName": "oil",
                    "destinationName": "line"
                },
                {
                    "sourceName": "oil1",
                    "destinationName": "line1"
                },
                {
                    "sourceName": "oil2",
                    "destinationName": "line2"
                }
            ]
        };

        $scope.currentChrodCollection = $scope.chrodCollection.collection1;




        var getCollection = function (collections, name) {
            var selection = {};
            collections.forEach(function (e, i) {
                if (e.name == name) {
                    selection = e;
                } else if (e.groups) {
                    selection = getCollection(e.groups, name);
                }
            });
            return selection;
        }

        $scope.currentCollection = $scope.dataCollections;
        $scope.currentcollectionName = $scope.currentCollection.name;




        $scope.menuClick = function (collection) {
            if (collection.name == 'dulang' || collection.name == 'pm') {
                $scope.dulangOccurrenceChart(collection.name);
            }
            $scope.currentCollection = collection;
            var currentLocationName = {
                'LocationName': $scope.currentCollection.name,
                'currentName': $scope.currentcollectionName,
                'showName': $scope.currentCollection.name
            }
            $scope.menuLists.push(currentLocationName);
            //calcCorr();
        }

        $scope.menuLists = [];

        $scope.mapClick = function (collection, name, value) {


            if (collection == 'apac') {
                $scope.currentcollectionName = $scope.dataCollections1.name;
                $scope.currentCollection = $scope.dataCollections1;
            } else if (collection == 'malaysia') {
                $scope.currentcollectionName = $scope.dataCollections2.name;
                $scope.currentCollection = $scope.dataCollections2;
            } else {
                $scope.currentcollectionName = $scope.dataCollections.name;
                $scope.currentCollection = $scope.dataCollections;
            }
            if (name != 'malaysia' && name != 'global' && name != 'apac') {
                $scope.currentCollection = getCollection($scope.currentCollection.groups, name);

            }
            if (value == 'true') {
                angular.forEach($scope.menuLists, function (i, ind) {

                    if (i.LocationName == name && i.currentName == collection) {
                        $scope.NewIndex = ind;
                    }
                })
                $scope.NewMenuList = [];

                angular.forEach($scope.menuLists, function (k, ind) {
                    if ($scope.NewIndex >= ind) {
                        $scope.NewMenuList.push(k);
                    }
                })

                $scope.menuLists.length = 0;
                $scope.menuLists = $scope.NewMenuList;
                // $scope.menuLists = $scope.menuLists.slice(0,$scope.menuLists.indexOf(name)-$scope.menuLists.length+1);
                // $scope.menuLists = $scope.menuLists.slice(0,$scope.menuLists.indexOf(name)+ $scope.menuLists.length-1 - $scope.menuLists.length++);
            } else {
                var currentLocationName = {
                    'LocationName': $scope.currentCollection.name,
                    'currentName': $scope.currentcollectionName,
                    'showName': $scope.currentcollectionName
                }
                $scope.menuLists.push(currentLocationName);
            }

            if ($scope.currentCollection.name == 'pm') {
                $scope.dulangOccurrenceChart('pm');
            }
            if (name == 'global' && collection == 'global') {
                $scope.menuLists = [];
            }

            //calcCorr();
        }




        $scope.showlalerttab = true;
        $scope.showalert = function () {
            $scope.showlalerttab = true;
            $scope.showwellheadtab = false;
            $scope.showsitetab = false;
        };

        //  $scope.showwellheadtab = false;
        // $scope.showwellhead = function(){
        //       $scope.showwellheadtab = true;
        //       $scope.showlalerttab = false;
        //        $scope.showsitetab = false;
        // };

        $scope.showsitetab = false;
        $scope.showsite = function () {
            $scope.showsitetab = true;
            $scope.showwellheadtab = false;
            $scope.showlalerttab = false;
            opupiechart();
        };


        $scope.showactivityViewtab = true;
        $scope.activityView = function () {
            $scope.gettableName('', 'activity')
        };

        $scope.showlocationViewtab = false;
        $scope.locationView = function () {
            $scope.showactivityViewtab = false;
            $scope.showlocationViewtab = true;
            $scope.showentityViewtab = false;
            $scope.showpeopleViewtab = false;
            $scope.corrTab = false;
            $scope.gettableName('', 'Specific location')
        };

        $scope.showentityViewtab = false;
        $scope.entityView = function () {
            $scope.showactivityViewtab = false;
            $scope.showlocationViewtab = false;
            $scope.showentityViewtab = true;
            $scope.showpeopleViewtab = false;
            $scope.corrTab = false;
            $scope.gettableName('', 'entity')
        };

        $scope.showpeopleViewtab = false;
        $scope.peopleView = function () {
            $scope.showactivityViewtab = false;
            $scope.showlocationViewtab = false;
            $scope.showentityViewtab = false;
            $scope.showpeopleViewtab = true;
            $scope.corrTab = false;
            $scope.gettableName('', 'people')
        };





        $scope.corrTab = false;
        $scope.blastClick = function () {
            $scope.corrTab = !$scope.corrTab;
        };
        $scope.myText1 = '';
        $scope.pitext = '';
        $scope.dropDownText = '';
        $scope.closeCorr = function () {
            $scope.corrTab = false;
            $scope.myText1 = '';
            $scope.showactivityViewtab = true;
            $scope.showlocationViewtab = false;
            $scope.showentityViewtab = false;
            $scope.showpeopleViewtab = false;
            $scope.highProbabilityName = 'high';
            $scope.showDropScoreDetail = false;
            $scope.topCatText = '10';
            $scope.pitext = 'All';
            $scope.dropDownText = 'high';
        };

        $scope.chartOpentab = true;
        $scope.monthlyChart = function () {
            $scope.chartOpentab = true;
            $scope.chartOpentab1 = false;
            $scope.chartOpentab2 = false;
            $scope.caseClick($scope.myText, 'monthly');

            getViewTrends();
            //corrChart();
        };

        $scope.chartOpentab1 = false;
        $scope.quarterlyChart = function () {
            $scope.chartOpentab = false;
            $scope.chartOpentab1 = true;
            $scope.chartOpentab2 = false;
            $scope.caseClick($scope.myText, 'quarterly');

            getViewTrends();
            //corrChart();
        };
        $scope.chartOpentab2 = false;
        $scope.yearlyChart = function () {
            $scope.chartOpentab = false;
            $scope.chartOpentab1 = false;
            $scope.chartOpentab2 = true;
            $scope.caseClick($scope.myText, 'yearly');

            getViewTrends();
            //corrChart();

        }


        //calcCorr();

        $scope.getmonthdataUA = [];
        $scope.getmonthdataUC = [];
        $scope.caseClick = function (myText, val) {
            $scope.myText = myText;

            if (val == 'monthly') {
                $scope.chartOpentab = true;
                $scope.chartOpentab1 = false;
                $scope.chartOpentab2 = false;
            }
            var url = appLinkFactory.getUrl('getGlobalCaseValue') + "?status=" + myText + "&filter=" + val + "&date=2017-06-01";
            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {
                $scope.ua = [];
                $scope.uc = [];
                $scope.legend = [];
                Object.keys(data).forEach(function (e, i) {
                    $scope.legend.push(e);
                    $scope.ua.push(data[e].ua);
                    $scope.uc.push(data[e].uc);
                })


                $scope.getchartdata = data;

                if ($scope.myText == 'Reported' || $scope.myText == 'Closed') {
                    $scope.cassesReportedClosed($scope.ua, $scope.uc, $scope.legend);
                }
                if ($scope.myText == 'Open') {
                   
                    var url = appLinkFactory.getUrl('getCasesOpenValue') + "?status=" + $scope.myText + "&date=2016-01-01";
                    var req = httpRequestService.connect(url, 'GET');
                    req.success(function (data) {
                        $scope.getCasesOpenValueCollection = data;
                        $scope.ua = [];
                        $scope.uc = [];
                        $scope.legend = [];
                        Object.keys(data).forEach(function (e, i) {
                            $scope.legend.push(e);
                            $scope.ua.push(data[e].ua);
                            $scope.uc.push(data[e].uc);
                        });
                        $scope.cassesOpen($scope.ua, $scope.uc, $scope.legend);
                    });
                    req.error(function (data) {
                    });
                }

            })
            req.error(function (err, msg) {

            })

        }

        $scope.cassesReportedClosed = function (UA, UC, val) {

            Highcharts.chart('trendChart', {
                chart: {
                    width: 540,
                    height: 300,
                    backgroundColor: '#fff'
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: ''
                },

                yAxis: {
                    title: {
                        text: 'Numbers'
                    }
                },
                xAxis: {
                    title: {
                        text: 'Months'
                    },
                    categories: val
                },
                exporting: { enabled: false },
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false,
                            color: '#f00'
                        },
                    }
                },

                series: [{
                    name: 'Unsafe Act',
                    data: UA,
                    color: '#00a9a2'

                }, {
                    name: 'Unsafe Condition',
                    data: UC,
                    color: '#454545'
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });

        }

        $scope.cassesOpen = function (UA, UC, val) {

            Highcharts.chart('caseChart', {
                chart: {
                    type: 'column',
                    backgroundColor: 'transparent',
                    width: 540,
                    height: 300
                },
                title: {
                    text: 'Aging of Cases' + " " + 'Opened',
                },
                exporting: { enabled: false },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['1 Week', '0 - 1 Month', '1 - 3 Months', '3 - 6 Months', '6 - 9 Months', '9 - 12 Months', '12 + Months'],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Numbers'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },

                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                    }
                },
                series: [{
                    name: 'UA',
                    data: [$scope.ua[0], $scope.ua[1], $scope.ua[2], $scope.ua[3], $scope.ua[4], $scope.ua[5], $scope.ua[6]],
                    color: '#00a9a2',
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },

                }, {
                    name: 'UC',
                    data: [$scope.uc[0], $scope.uc[1], $scope.uc[2], $scope.uc[3], $scope.uc[4], $scope.uc[5], $scope.uc[6]],
                    color: '#454545',
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                        }
                    },

                }]
            });

        }


           
            


        $scope.dulangPopOccurrenceChart = function (val) {
            var newType;
            var charname;
            if (val == 'pm') {
                newType = 'pma';
                charname = 'bubblechart2';
            } else {
                newType = val;
                charname = 'bubblechart3';
            }

            //var url = appLinkFactory.getUrl('Getquadrantplotdata') + "?type=" + newType;

            //var req = httpRequestService.connect(url, 'GET');
            // req.success(function (data) {


            Highcharts.chart(charname, {

                chart: {
                    type: 'bubble',
                    plotBorderWidth: 1,
                    zoomType: 'xy',
                    height: '500',
                    backgroundColor: 'transparent'
                },
                exporting: { enabled: false },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },

                xAxis: {
                    gridLineWidth: 1,
                    title: {
                        text: 'Incident (Occurrence)'
                    },
                    plotLines: [{
                        color: '#f00',
                        dashStyle: 'solid',
                        width: 2,
                        value: 0.5,

                        zIndex: 3
                    }]
                },

                yAxis: {
                    startOnTick: false,
                    endOnTick: false,
                    title: {
                        text: 'UAUC (Observation)'
                    },
                    maxPadding: 0.2,
                    plotLines: [{
                        color: '#f00',
                        dashStyle: 'solid',
                        width: 2,
                        value: 0.5,

                        zIndex: 3
                    }]
                },

                tooltip: {
                    useHTML: true,
                    headerFormat: '<table>',
                    pointFormat:
                        '<tr><td>{point.name}</td></tr>',
                    footerFormat: '</table>',
                    followPointer: true
                },
                plotOptions: {
                    bubble: {
                        minSize: 10,
                        maxSize: 10,
                        events: {
                            click: function (e) {
                                alert(e);
                            }
                        }
                    }
                },

                series: [{
                    data: $scope.dulangOccurrenceX
                }]

            });

            // })
            // req.error(function (err, msg) {

            // })


        }
        //dulang chart

        $scope.dulangOccurrenceChart = function (val) {
            var newType;
            var charname;
            if (val == 'pm') {
                newType = 'pma';
                charname = 'bubblechart1';
            } else {
                newType = val;
                charname = 'bubblechart';
            }

            var url = appLinkFactory.getUrl('Getquadrantplotdata') + "?type=" + newType;

            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {

                $scope.dulangOccurrenceX = [];

                Object.keys(data).forEach(function (e, i) {
                    data[e].forEach(function (f, j) {
                        if (e == 'Activity') {
                            f.x = f.xaxis;
                            f.y = f.yaxis;
                            f.z = '1';
                            f.name = f.element;
                            f.color = '#9c27b0';
                        } else if (e == 'Entity') {
                            f.x = f.xaxis;
                            f.y = f.yaxis;
                            f.z = '1';
                            f.name = f.element;
                            f.color = '#777';
                        } else if (e == 'Location') {
                            f.x = f.xaxis;
                            f.y = f.yaxis;
                            f.z = '1';
                            f.name = f.element;
                            f.color = 'blue';
                        } else if (e == 'People') {
                            f.x = f.xaxis;
                            f.y = f.yaxis;
                            f.z = '1';
                            f.name = f.element;
                            f.color = '#2196f3';
                        }
                        $scope.dulangOccurrenceX.push(f);
                    })
                })


                Highcharts.chart(charname, {

                    chart: {
                        type: 'bubble',
                        plotBorderWidth: 1,
                        zoomType: 'xy',
                        height: '260',
                        backgroundColor: 'transparent'
                    },
                    exporting: { enabled: false },
                    legend: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },

                    subtitle: {
                        text: ''
                    },

                    xAxis: {
                        gridLineWidth: 1,
                        title: {
                            text: 'Incident (Occurrence)'
                        },
                        plotLines: [{
                            color: '#f00',
                            dashStyle: 'solid',
                            width: 2,
                            value: 0.5,
                            zIndex: 999
                        }]
                    },

                    yAxis: {
                        startOnTick: false,
                        endOnTick: false,
                        title: {
                            text: 'UAUC (Observation)'
                        },
                        maxPadding: 0.2,
                        plotLines: [{
                            color: '#f00',
                            dashStyle: 'solid',
                            width: 2,
                            value: 0.5,
                            zIndex: 999
                        }]
                    },

                    tooltip: {
                        useHTML: true,
                        headerFormat: '<table>',
                        pointFormat:
                            '<tr><td>{point.name}</td></tr>',
                        footerFormat: '</table>',
                        followPointer: true
                    },
                    plotOptions: {
                        bubble: {
                            minSize: 10,
                            maxSize: 10,
                            events: {
                                click: function () {
                                    $('#Chart1').modal('show');
                                }
                            }
                        }
                    },

                    series: [{
                        data: $scope.dulangOccurrenceX
                    }]

                });

            })
            req.error(function (err, msg) {

            })



        }

        $scope.dulangOccurrenceChart('pm');

        var getViewTrends = function () {
            Highcharts.chart('viewTrendChart', {
                chart: {
                    type: 'area',
                    backgroundColor: 'transparent'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    tickmarkPlacement: 'on',
                    title: {
                        enabled: false
                    }
                },
                yAxis: {
                    title: {
                        text: 'Numbers'
                    },
                    labels: {
                        formatter: function () {
                            return this.value / 1000;
                        }
                    }
                },
                tooltip: {
                    split: true,
                    valueSuffix: ' millions'
                },
                exporting: { enabled: false },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        lineColor: '#666666',
                        lineWidth: 1,
                        marker: {
                            lineWidth: 1,
                            lineColor: '#666666'
                        }
                    }
                },
                series: [{
                    name: 'UPSTREAM',
                    data: [5012, 6325, 8039, 9427, 102, 3634, 5268]
                }, {
                    name: 'DOWNSTREAM',
                    data: [1206, 1047, 1141, 1333, 2521, 7767, 1766]
                }, {
                    name: 'PD&T',
                    data: [1613, 2034, 2765, 4084, 5472, 7292, 6288]
                }, {
                    name: 'MISC',
                    data: [1811, 3122, 5334, 156, 3439, 9818, 1701]
                }, {
                    name: 'OTHERS',
                    data: [2, 2, 2, 6, 13, 30, 46]
                }]
            });
        };

        // active getViewTrends 

        //Highcharts.chart('viewTrendChart', {
        //    chart: {
        //        type: 'area',
        //        backgroundColor: 'transparent'
        //    },
        //    title: {
        //        text: ''
        //    },
        //    subtitle: {
        //        text: ''
        //    },
        //    xAxis: {
        //        categories: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        //        tickmarkPlacement: 'on',
        //        title: {
        //            enabled: false
        //        }
        //    },
        //    yAxis: {
        //        title: {
        //            text: 'Numbers'
        //        },
        //        labels: {
        //            formatter: function () {
        //                return this.value / 1000;
        //            }
        //        }
        //    },
        //    tooltip: {
        //        split: true,
        //        valueSuffix: ' millions'
        //    },
        //    exporting: { enabled: false },
        //    credits: {
        //        enabled: false
        //    },
        //    plotOptions: {
        //        area: {
        //            stacking: 'normal',
        //            lineColor: '#666666',
        //            lineWidth: 1,
        //            marker: {
        //                lineWidth: 1,
        //                lineColor: '#666666'
        //            }
        //        }
        //    },
        //    series: [{
        //        name: 'UPSTREAM',
        //        data: [5012, 6325, 8039, 9427, 102, 3634, 5268]
        //    }, {
        //        name: 'DOWNSTREAM',
        //        data: [1206, 1047, 1141, 1333, 2521, 7767, 1766]
        //    }, {
        //        name: 'PD&T',
        //        data: [1613, 2034, 2765, 4084, 5472, 7292, 6288]
        //    }, {
        //        name: 'MISC',
        //        data: [1811, 3122, 5334, 156, 3439, 9818, 1701]
        //    }, {
        //        name: 'OTHERS',
        //        data: [2, 2, 2, 6, 13, 30, 46]
        //    }]
        //});


        // corrChart active
        Highcharts.chart('corrChart', {
            chart: {
                type: 'area',
                backgroundColor: 'transparent',
                height: 220
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                tickmarkPlacement: 'on',
                title: {
                    enabled: false
                }
            },
            yAxis: {
                title: {
                    text: 'Numbers'
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000;
                    }
                }
            },
            tooltip: {
                split: true,
                valueSuffix: ' millions'
            },
            exporting: { enabled: false },
            credits: {
                enabled: false
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: [{
                name: 'UPSTREAM',
                data: [5012, 6325, 8039, 9427, 102, 3634, 5268]
            }]
        });



        Highcharts.chart('corrChart1', {
            chart: {
                type: 'area',
                backgroundColor: 'transparent',
                height: 220
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                tickmarkPlacement: 'on',
                title: {
                    enabled: false
                }
            },
            yAxis: {
                title: {
                    text: 'Numbers'
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000;
                    }
                }
            },
            tooltip: {
                split: true,
                valueSuffix: ' millions'
            },
            exporting: { enabled: false },
            credits: {
                enabled: false
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: [{
                name: 'UPSTREAM',
                data: [5012, 6325, 8039, 9427, 102, 3634, 5268]
            }]
        });


        var corrChart = function () {
            Highcharts.chart('corrChart', {
                chart: {
                    type: 'area',
                    backgroundColor: 'transparent',
                    height: 220
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
                    tickmarkPlacement: 'on',
                    title: {
                        enabled: false
                    }
                },
                yAxis: {
                    title: {
                        text: 'Numbers'
                    },
                    labels: {
                        formatter: function () {
                            return this.value / 1000;
                        }
                    }
                },
                tooltip: {
                    split: true,
                    valueSuffix: ' millions'
                },
                exporting: { enabled: false },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        lineColor: '#666666',
                        lineWidth: 1,
                        marker: {
                            lineWidth: 1,
                            lineColor: '#666666'
                        }
                    }
                },
                series: [{
                    name: 'UPSTREAM',
                    data: [5012, 6325, 8039, 9427, 102, 3634, 5268]
                }]
            });
        }



        var pieChartNew = function () {

            // Create the chart
            Highcharts.chart('piechart1', {
                chart: {
                    type: 'pie',
                    height: '170px'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:2f}%</b> of total<br/>'
                },
                series: [{
                    name: '',
                    colorByPoint: true,
                    data: [{
                        name: 'High',
                        y: 56.33,
                        color: '#f00'
                    }, {
                        name: 'Medium',
                        y: 24.03,
                        color: '#ff7300'
                    }, {
                        name: 'Low',
                        y: 10.38,
                        color: '#c7c7c7'
                    }]
                }],
                exporting: { enabled: false },
                credits: {
                    enabled: false
                },
                drilldown: {
                    series: [{
                        name: 'High',
                        id: 'High',
                        data: [
                            ['High', 24.13],
                            ['Medium', 17.2],
                            ['Low', 8.11],
                        ]
                    }, {
                        name: 'Medium',
                        id: 'Medium',
                        data: [
                            ['High', 5],
                            ['Low', 4.32],
                            ['Medium', 3.68],
                        ]
                    }, {
                        name: 'Low',
                        id: 'Low',
                        data: [
                            ['Low', 2.76],

                        ]
                    }]
                }
            });
        }



        // Create the chart



        //var opupiechart = function () {
        //    Highcharts.chart('opupiechart', {
        //        chart: {
        //            plotBackgroundColor: null,
        //            plotBorderWidth: null,
        //            plotShadow: false,
        //            type: 'pie',
        //            backgroundColor: 'transparent',
        //            height: '220px'
        //        },
        //        title: {
        //            text: ''
        //        },
        //        tooltip: {
        //            pointFormat: '{series.name}: <b>{point.percentage:1f}%</b>'
        //        },
        //        plotOptions: {
        //            pie: {
        //                allowPointSelect: true,
        //                cursor: 'pointer',
        //                connectorWidth: 0,
        //                dataLabels: {
        //                    enabled: true,
        //                    format: '<b>{point.name}</b>: {point.percentage:1f} %',
        //                    connectorWidth: 0,
        //                    style: {
        //                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        //                    }
        //                }
        //            }
        //        },
        //        exporting: { enabled: false },
        //        credits: {
        //            enabled: false
        //        },
        //        series: [{
        //            name: '',
        //            colorByPoint: true,
        //            data: [{
        //                name: 'FIRE',
        //                y: 40
        //            }, {
        //                name: 'PERSONAL INJURY',
        //                y: 20,
        //            }, {
        //                name: 'LOPC',
        //                y: 40
        //            }]
        //        }]
        //    });
        //}

        //Highcharts.chart('opupiechart', {
        //    chart: {
        //        plotBackgroundColor: null,
        //        plotBorderWidth: null,
        //        plotShadow: false,
        //        type: 'pie',
        //        backgroundColor: 'transparent',
        //        height: '220px'
        //    },
        //    title: {
        //        text: ''
        //    },
        //    tooltip: {
        //        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //    },
        //    plotOptions: {
        //        pie: {
        //            allowPointSelect: true,
        //            cursor: 'pointer',
        //            dataLabels: {
        //                enabled: true,
        //                format: '<b>{point.name}</b>: {point.percentage:1f} %',
        //                connectorWidth: 0,
        //                style: {
        //                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        //                }
        //            }
        //        }
        //    },
        //    exporting: { enabled: false },
        //    credits: {
        //        enabled: false
        //    },
        //    series: [{
        //        name: '',
        //        colorByPoint: true,
        //        data: [{
        //            name: 'FIRE',
        //            y: 40
        //        }, {
        //            name: 'PERSONAL INJURY',
        //            y: 20,
        //        }, {
        //            name: 'LOPC',
        //            y: 40
        //        }]
        //    }]
        //});

        $(function () {
            Highcharts.setOptions({ lang: { drillUpText: '< Back' } });
            // Create the chart
            $('#grouppiechart').highcharts({
                chart: {
                    type: 'pie',
                    backgroundColor: 'transparent',
                    height: '220px'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:1f}%',
                            connectorWidth: 0
                        },
                    },
                },
                exporting: { enabled: false },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:2f}%</b> of total<br/>'
                },

                series: [{
                    name: ' ',
                    data: [{
                        name: 'PERSONAL INJURY',
                        y: 50,
                        drilldown: 'PI'
                    }, {
                        name: 'FIRE',
                        y: 20,
                        drilldown: 'FI'
                    }, {
                        name: 'LOPC',
                        y: 30,
                        drilldown: 'LOPC1'
                    }]
                }],
                drilldown: {
                    series: [{
                        id: 'PI',
                        data: [
                            { name: 'High', y: 20, color: 'red' },
                            { name: 'Medium', y: 40, color: '#ff7300' },
                            { name: 'Low', y: 40, color: '#b8b7b7' }
                        ],
                    }, {
                        id: 'FI',
                        data: [
                            { name: 'High', y: 50, color: 'red' },
                            { name: 'Medium', y: 40, color: '#ff7300' },
                            { name: 'Low', y: 10, color: '#b8b7b7' }
                        ],
                    }, {
                        id: 'LOPC1',
                        data: [
                            { name: 'High', y: 30, color: 'red' },
                            { name: 'Medium', y: 40, color: '#ff7300' },
                            { name: 'Low', y: 30, color: '#b8b7b7' }
                        ]
                    }]
                }
            });





          


            $scope.getUpstreamCaseValue = function () {
                var url = appLinkFactory.getUrl('getGlobalCaseValue') + "?date=2016-01-31";

                var req = httpRequestService.connect(url, 'GET');
                req.success(function (data) {
                    $scope.uaucCollections = data;
                    $scope.uaucRoundValueUa = $scope.uaucCollections.Reported.ua / ($scope.uaucCollections.Reported.ua + $scope.uaucCollections.Reported.uc) * 100;
                    $scope.uaucRoundValueUc = $scope.uaucCollections.Reported.uc / ($scope.uaucCollections.Reported.ua + $scope.uaucCollections.Reported.uc) * 100;

                    $scope.uaucRoundValueClosedUa = $scope.uaucCollections.Closed.ua / ($scope.uaucCollections.Closed.ua + $scope.uaucCollections.Closed.uc) * 100;
                    $scope.uaucRoundValueClosedUc = $scope.uaucCollections.Closed.uc / ($scope.uaucCollections.Closed.ua + $scope.uaucCollections.Closed.uc) * 100;

                    $scope.uaucRoundValueOpenedUa = $scope.uaucCollections.Open.ua / ($scope.uaucCollections.Open.ua + $scope.uaucCollections.Open.uc) * 100;

                    $scope.uaucRoundValueOpenedUc = $scope.uaucCollections.Open.uc / ($scope.uaucCollections.Open.ua + $scope.uaucCollections.Open.uc) * 100;



                    Highcharts.chart('casesReportedBar', {
                        chart: {
                            type: 'bar',
                            width: 320,
                            height: 40,
                            backgroundColor: 'transparent'
                        },
                        tooltip: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            visible: false,
                            gridLineColor: 'transparent',
                            labels: {
                                enabled: false
                            },

                        },
                        yAxis: {
                            min: 0,
                            gridLineColor: 'transparent',
                            title: {
                                text: ''
                            },
                            labels: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        exporting: { enabled: false },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            borderColor: 'transparent',
                            borderWidth: 0,
                            name: 'UA',
                            data: [$scope.uaucRoundValueUc],
                            color: '#454545'

                        }, {
                            borderColor: 'transparent',
                            borderWidth: 0,
                            name: 'UC',
                            data: [$scope.uaucRoundValueUa],
                            color: '#00a9a2'
                        }]
                    });


                    Highcharts.chart('casesClosedBar', {
                        chart: {
                            type: 'bar',
                            width: 320,
                            height: 40,
                            backgroundColor: 'transparent'
                        },
                        tooltip: {
                            enabled: false
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            visible: false,
                            gridLineColor: 'transparent',
                            labels: {
                                enabled: false
                            },

                        },
                        yAxis: {
                            min: 0,
                            gridLineColor: 'transparent',
                            title: {
                                text: ''
                            },
                            labels: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        exporting: { enabled: false },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            borderColor: 'transparent',
                            borderWidth: 0,
                            name: 'UA',
                            data: [$scope.uaucRoundValueClosedUc],
                            color: '#454545'
                        }, {
                            borderColor: 'transparent',
                            borderWidth: 0,
                            name: 'UC',
                            data: [$scope.uaucRoundValueClosedUa],
                            color: '#00a9a2'

                        }]
                    });

                    Highcharts.chart('casesOpenedBar', {
                        chart: {
                            type: 'bar',
                            width: 320,
                            height: 40,
                            backgroundColor: 'transparent'
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            visible: false,
                            gridLineColor: 'transparent',
                            labels: {
                                enabled: false
                            },

                        },
                        yAxis: {
                            min: 0,
                            gridLineColor: 'transparent',
                            title: {
                                text: ''
                            },
                            labels: {
                                enabled: false
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        exporting: { enabled: false },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            borderColor: 'transparent',
                            borderWidth: 0,
                            name: 'UA',
                            data: [$scope.uaucRoundValueOpenedUc],
                            color: '#454545'
                        }, {
                            borderColor: 'transparent',
                            borderWidth: 0,
                            name: 'UC',
                            data: [$scope.uaucRoundValueOpenedUa],
                            color: '#00a9a2'
                        }]
                    });


                })
                req.error(function (err, msg) {

                })
            }


            $scope.getUpstreamCaseValue();



            $scope.getDulangProbability = function () {
                var url = appLinkFactory.getUrl('getDulangProbability') + "?frondatetime=2015-07-01" + "&todatetime=2017-07-01";

                var req = httpRequestService.connect(url, 'GET');
                req.success(function (data) {
                    $scope.getDulangProbability = data;

                    $scope.splitHigh = $scope.getDulangProbability[0].high;
                    $scope.splitMed = $scope.getDulangProbability[1].medium;
                    $scope.splitLow = $scope.getDulangProbability[2].low;

                })
                req.error(function (err, msg) {

                })
            }

            $scope.getDulangProbability();


            $scope.gettableName = function (val, type) {
                $scope.groupLevelTittle = type;
                $scope.filterByTopList();
                if ($scope.myText1 == '') {
                    var url = appLinkFactory.getUrl('Getcorrelationscoredulangdata') + "?element_type=" + type + "&correlation=" + $scope.highProbabilityName + "&fromDateTime=2015-07-01&toDateTime=2018-07-31";
                } else {
                    var url = appLinkFactory.getUrl('Getcorrelationscoredulangdata') + "?element_type=" + type + "&correlation=" + $scope.highProbabilityName + "&site=" + $scope.myText1 + "&fromDateTime=2015-07-01&toDateTime=2018-07-31";
                }
                var Getcorrelationscoredulangdata = httpRequestService.connect(url, 'GET');
                Getcorrelationscoredulangdata.success(function (data) {

                    $scope.correlationMap = [];
                    if (type == 'activity') {
                        Object.keys(data.Activity).forEach(function (e, i) {

                            if (e.indexOf('Loss') > -1) {
                                $scope.correlationMap.push({
                                    name: "LOPC",
                                    children: []
                                });
                            }
                            else {
                                $scope.correlationMap.push({
                                    name: e,
                                    children: []
                                });
                            }

                            data.Activity[e].forEach(function (f, j) {
                                $scope.correlationMap[i].children.push({
                                    name: f.element + "(" + f.element_count + ")",

                                })
                            })

                        })
                    } else if (type == 'people') {
                        Object.keys(data.People).forEach(function (e, i) {


                            if (e.indexOf('Loss') > -1) {
                                $scope.correlationMap.push({
                                    name: "LOPC",
                                    children: []
                                });
                            }
                            else {
                                $scope.correlationMap.push({
                                    name: e,
                                    children: []
                                });
                            }
                            data.People[e].forEach(function (f, j) {
                                $scope.correlationMap[i].children.push({
                                    name: f.element + "(" + f.element_count + ")",

                                })
                            })

                        })
                    } else if (type == 'Specific location') {

                        Object.keys(data['Specific Location']).forEach(function (e, i) {


                            if (e.indexOf('Loss') > -1) {
                                $scope.correlationMap.push({
                                    name: "LOPC",
                                    children: []
                                });
                            }
                            else {
                                $scope.correlationMap.push({
                                    name: e,
                                    children: []
                                });
                            }
                            data['Specific Location'][e].forEach(function (f, j) {
                                $scope.correlationMap[i].children.push({
                                    name: f.element + "(" + f.element_count + ")",

                                })
                            })

                        })
                    } else if (type == 'entity') {

                        Object.keys(data.Entity).forEach(function (e, i) {


                            if (e.indexOf('Loss') > -1) {
                                $scope.correlationMap.push({
                                    name: "LOPC",
                                    children: []
                                });
                            }
                            else {
                                $scope.correlationMap.push({
                                    name: e,
                                    children: []
                                });
                            }
                            data.Entity[e].forEach(function (f, j) {
                                $scope.correlationMap[i].children.push({
                                    name: f.element + "(" + f.element_count + ")",

                                })
                            })

                        })
                    }
                    root = $scope.correlationMap[0];
                    $scope.currentCorrelationMap = 0;
                    update(root);
                })
                Getcorrelationscoredulangdata.error(function (err, msg) {

                })
            }


            var margin = {
                top: 120,
                right: 120,
                bottom: 20,
                left: 90
            },
            width = 700 - margin.right - margin.left,
            height = 400 - margin.top - margin.bottom;

            var root = {
                "name": "ACTIVITY",
                "children": [{
                    "name": "Blasting" + " " + "30",
                }, {
                    "name": "Paiting" + " " + "30",

                }, {
                    "name": "WELDING" + " " + "30",
                }, {
                    "name": "LIFTING" + " " + "30",
                }, {
                    "name": "Fire" + " " + "30",
                }, {
                    "name": "LOPC" + " " + "30",

                }]
            };

            var i = 0,
                duration = 750,
                rectW = 100,
                rectH = 30;

            var tree = d3.layout.tree().nodeSize([105, 40]);
            var diagonal = d3.svg.diagonal()
                .projection(function (d) {
                    return [d.x + rectW / 2, d.y + rectH / 2];
                });

            var svg = d3.select("#body").append("svg").attr("width", 1200).attr("height", 400)
                .call(zm = d3.behavior.zoom().scaleExtent([1, 3]).on("zoom", null)).append("g")
                .attr("transform", "translate(" + 500 + "," + 20 + ")");

            //necessary so that zoom knows where to zoom and unzoom from
            zm.translate([300, 20]);

            root.x0 = 0;
            root.y0 = height / 2;

            function collapse(d) {
                if (d.children) {
                    d._children = d.children;
                    d._children.forEach(collapse);
                    d.children = null;
                }
            }

            root.children.forEach(collapse);
            update(root);

            d3.select("#body").style("height", "800px");

            function update(source) {

                // Compute the new tree layout.
                var nodes = tree.nodes(root).reverse(),
                    links = tree.links(nodes);

                // Normalize for fixed-depth.
                nodes.forEach(function (d) {
                    d.y = d.depth * 180;
                });

                // Update the nodesâ€¦
                var node = svg.selectAll("g.node")
                    .data(nodes, function (d) {
                        return d.id || (d.id = ++i);
                    });

                // Enter any new nodes at the parent's previous position.
                var nodeEnter = node.enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function (d) {
                        return "translate(" + source.x0 + "," + source.y0 + ")";
                    })
                    .on("click", click);

                nodeEnter.append("rect")
                    .attr("width", rectW)
                    .attr("height", rectH)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .style("fill", function (d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });

                nodeEnter.append("text")
                    .attr("x", rectW / 2)
                    .attr("y", rectH / 2)
                    .attr("dy", ".35em")
                    .attr("text-anchor", "middle")
                    .text(function (d) {
                        return d.name;
                    });

                // Transition nodes to their new position.
                var nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });

                nodeUpdate.select("rect")
                    .attr("width", rectW)
                    .attr("height", rectH)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .style("fill", function (d) {
                        return d._children ? "lightsteelblue" : "#fff";
                    });

                nodeUpdate.select("text")
                    .style("fill-opacity", 1);

                // Transition exiting nodes to the parent's new position.
                var nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + source.x + "," + source.y + ")";
                    })
                    .remove();

                nodeExit.select("rect")
                    .attr("width", rectW)
                    .attr("height", rectH)
                //.attr("width", bbox.getBBox().width)""
                //.attr("height", bbox.getBBox().height)
                .attr("stroke", "black")
                    .attr("stroke-width", 1);

                nodeExit.select("text");

                // Update the linksâ€¦
                var link = svg.selectAll("path.link")
                    .data(links, function (d) {
                        return d.target.id;
                    });

                // Enter any new links at the parent's previous position.
                link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("x", rectW / 2)
                    .attr("y", rectH / 2)
                    .attr("d", function (d) {
                        var o = {
                            x: source.x0,
                            y: source.y0
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    });

                // Transition links to their new position.
                link.transition()
                    .duration(duration)
                    .attr("d", diagonal);

                // Transition exiting nodes to the parent's new position.
                link.exit().transition()
                    .duration(duration)
                    .attr("d", function (d) {
                        var o = {
                            x: source.x,
                            y: source.y
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .remove();

                // Stash the old positions for transition.
                nodes.forEach(function (d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                });
            }

            // Toggle children on click.
            function click(d) {
                $scope.incidenttype = $scope.correlationMap[$scope.currentCorrelationMap].name;
                var tempSplitdata = d.name.split('(');
                if (tempSplitdata[1] != undefined) {
                    $scope.NewSplitData = tempSplitdata[0];
                    $scope.GetDulangUAUCCatPercentage();
                    $scope.corrTab = true;
                    correlationTable($scope.highProbabilityName);
                    $scope.$apply();
                }

            }

            //Redraw for zoom
            function redraw() {
                //console.log("here", d3.event.translate, d3.event.scale);
                svg.attr("transform",
                    "translate(" + d3.event.translate + ")"
                    + " scale(" + d3.event.scale + ")");
            }



            $scope.highProbabilityName = 'high';

            $scope.highProbability = function (val) {
                // correlationTable(val)
                $scope.highProbabilityName = val;
                $scope.corrTab = false;
                $scope.gettableName('', $scope.groupLevelTittle);
            }
            $scope.correlationTableValue = [];
            $scope.correlationTable = function () {
                $scope.correlationTableValue = [];
                if ($scope.myText1 == '') {
                    if ($scope.incidenttype == "All") {
                        var url = appLinkFactory.getUrl('getdulanguaucscoredata') + "?element=" + $scope.elementText + "&incident_type =" + $scope.incidenttype + "&correlation=" + $scope.dropDownText + "&pageNo=1&fromDateTime=2015-07-01&toDateTime=2017-07-31";
                        if ($scope.myText1 != '') {
                            url += "&site=" + $scope.myText1;
                        }
                    }
                    else {
                        var url = appLinkFactory.getUrl('getdulanguaucscoredata') + "?element=" + $scope.elementText + "&incident_type=" + $scope.incidenttype + "&correlation=" + $scope.dropDownText + "&pageNo=1&fromDateTime=2015-07-01&toDateTime=2017-07-31";
                        if ($scope.myText1 != '') {
                            url += "&site=" + $scope.myText1;
                        }
                    }
                } else {
                    if ($scope.incidenttype == "All") {
                        var url = appLinkFactory.getUrl('getdulanguaucscoredata') + "?element=" + $scope.elementText + "&incident_type =" + $scope.incidenttype + "&correlation=" + $scope.dropDownText + "&pageNo=1&fromDateTime=2015-07-01&toDateTime=2017-07-31";
                      
                        if ($scope.myText1 != '') {
                            url += "&site=" + $scope.myText1;
                        }
                    }
                    else {
                        if ($scope.myText1 == '') {
                            var url = appLinkFactory.getUrl('getdulanguaucscoredata') + "?element=" + $scope.elementText + "&incident_type =" + $scope.incidenttype + "&correlation=" + $scope.dropDownText + "&pageNo=1&fromDateTime=2015-07-01&toDateTime=2017-07-31";
                        }
                       
                        if ($scope.myText1 != '') {
                            var url = appLinkFactory.getUrl('getdulanguaucscoredata') + "?element=" + $scope.elementText + "&incident_type=" + $scope.incidenttype + "&correlation=" + $scope.dropDownText + "&pageNo=1&fromDateTime=2015-07-01&toDateTime=2017-07-31";
                             url += "&site=" + $scope.myText1;
                        }
                    }
                }
                var req = httpRequestService.connect(url, 'GET');
                req.success(function (data) {
                    $scope.correlationTableValue = data;
                    angular.forEach($scope.correlationTableValue, function (i) {
                        i.show = false;
                    });

                })
                req.error(function (err, msg) {

                })
            }

            $scope.showtabdescription = function (val, index) {

                angular.forEach($scope.correlationTableValue, function (i) {
                    i.show = false;
                })

                angular.forEach($scope.correlationTableValue, function (i, ind) {
                    if (ind == index) {
                        i.show = val == false ? true : false;
                    }

                })

            }

            $scope.GetDulangUAUCCatPercentage = function () {
                if ($scope.myText1 == '') {
                    var url = appLinkFactory.getUrl('GetDulangUAUCCatPercentage') + "?element_type=" + $scope.groupLevelTittle + "&incident_type=" + $scope.incidenttype + "&correlation=" + $scope.highProbabilityName + "&element=" + $scope.NewSplitData + "&site=DULANG A&fromDateTime=2017-07-01&toDateTime=2017-07-31";
                   
                } else {
                    var url = appLinkFactory.getUrl('GetDulangUAUCCatPercentage') + "?element_type=" + $scope.groupLevelTittle + "&incident_type=" + $scope.incidenttype + "&correlation=" + $scope.highProbabilityName + "&element=" + $scope.NewSplitData + "&site=" + $scope.myText1 + "&fromDateTime=2017-07-01&toDateTime=2017-07-31";
                    if ($scope.myText1 != '') {
                        url += "&site=" + $scope.myText1;
                    }
                }
                var req = httpRequestService.connect(url, 'GET');
                req.success(function (data) {
                    $scope.GetDulangUAUCCatPercentageValue = [];
                    data.forEach(function (e, i) {
                        $scope.GetDulangUAUCCatPercentageValue.push({
                            "name": e.categories,
                            "y": e.percentage
                        });
                    });

                    // correlation pie chart
                    Highcharts.chart('correlated_chart', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie',
                            backgroundColor: 'transparent',
                            height: '320px'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                connectorWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                                    connectorWidth: 0,
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                    }
                                }
                            }
                        },
                        exporting: { enabled: false },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: '',
                            colorByPoint: true,
                            data: $scope.GetDulangUAUCCatPercentageValue
                        }]
                    });



                })
                req.error(function (err, msg) {

                })
            }



        });




        $scope.prevCorrelation = function () {
            $scope.corrTab = false;
            if ($scope.currentCorrelationMap - 1 < 0) {
                $scope.currentCorrelationMap = $scope.correlationMap.length;

            }
            root = $scope.correlationMap[--$scope.currentCorrelationMap];
            update(root);
        }

        $scope.nextCorrelation = function () {
            $scope.corrTab = false;
            if ($scope.currentCorrelationMap + 1 >= $scope.correlationMap.length) {
                $scope.currentCorrelationMap = -1;

            }
            root = $scope.correlationMap[++$scope.currentCorrelationMap];
            update(root);
        }





        $scope.GetOPULevel = function () {
            var url = appLinkFactory.getUrl('GetOPULevelPercentage') + "?fromDateTime=" +"2017-01-01" + "&toDateTime=" +"2017-07-31";
            var req = httpRequestService.connect(url, 'GET');

            req.success(function (data) {
                $scope.getOPUPercentage = data;

                //$scope.uaucRoundValueUa = $scope.uaucCollections.Reported.ua / ($scope.uaucCollections.Reported.ua + $scope.uaucCollections.Reported.uc) * 100;
                //   var total = $scope.getOPUPercentage[0].percentage + $scope.getOPUPercentage[1].percentage + $scope.getOPUPercentage[2].percentage;
                $scope.getOPUPercentageTotal = $scope.getOPUPercentage[0].percentage + $scope.getOPUPercentage[1].percentage + $scope.getOPUPercentage[2].percentage;

                Highcharts.chart('opuLevelBar', {
                    chart: {
                        type: 'bar',
                        width: 320,
                        height: 40,
                        backgroundColor: 'transparent'
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        visible: false,
                        gridLineColor: 'transparent',
                        labels: {
                            enabled: false
                        },

                    },
                    yAxis: {
                        min: 0,
                        gridLineColor: 'transparent',
                        title: {
                            text: ''
                        },
                        labels: {
                            enabled: false
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    exporting: { enabled: false },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: $scope.getOPUPercentage[0].incident_type,
                        data: [$scope.getOPUPercentage[0].percentage],
                        borderColor: 'transparent',
                        borderWidth: 0,
                        color: '#454545'
                    }, {
                        name: $scope.getOPUPercentage[1].incident_type,
                        data: [$scope.getOPUPercentage[1].percentage],
                        borderColor: 'transparent',
                        borderWidth: 0,
                        color: '#00a9a2'
                    }, {
                        name: $scope.getOPUPercentage[2].incident_type,
                        data: [$scope.getOPUPercentage[2].percentage],
                        borderColor: 'transparent',
                        borderWidth: 0,
                        color: '#989898'
                    }]
                });
            })
            req.error(function (err, msg) {

            })
        }

        $scope.GetOPULevel();






        $scope.GetDulangPieChart = function () {
            var url = appLinkFactory.getUrl('GetDulangPie') + "?fromDateTime=2015-07-01&toDateTime=2017-07-31";
            var req = httpRequestService.connect(url, 'GET');



            req.success(function (data) {
                $scope.GetDulangPieChart1 = data;
                var url = appLinkFactory.getUrl('GetDulangPieDrillDown') + "?correlation=high&fromDateTime=2015-07-01&toDateTime=2017-07-31";
                var req = httpRequestService.connect(url, 'GET');
                req.success(function (data) {

                    $scope.GetDulangPieChart2 = data;

                    //$('#grouppiechartsitelevel').highcharts({
                    //    chart: {
                    //        type: 'pie',
                    //        backgroundColor: 'transparent',
                    //        height: '220px'
                    //    },
                    //    title: {
                    //        text: ''
                    //    },
                    //    subtitle: {
                    //        text: ''
                    //    },
                    //    plotOptions: {
                    //        series: {
                    //            dataLabels: {
                    //                enabled: true,
                    //                format: '{point.name}: <br> {point.y:1f}%',
                    //                connectorWidth: 0
                    //            },
                    //        },
                    //    },
                    //    exporting: { enabled: false },
                    //    credits: {
                    //        enabled: false
                    //    },
                    //    tooltip: {
                    //        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    //        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:2f}%</b> of total<br/>'
                    //    },

                    //    series: [{
                    //        name: ' ',
                    //        data: [{
                    //            name: 'HIGH',
                    //            y: $scope.GetDulangPieChart1[0].high,
                    //            drilldown: 'sitehigh',
                    //            color: 'red'
                    //        }, {
                    //            name: 'MEDIUM',
                    //            y: $scope.GetDulangPieChart1[1].medium,
                    //            drilldown: 'sitemedium',
                    //            color: '#ff7300'
                    //        }, {
                    //            name: 'LOW',
                    //            y: $scope.GetDulangPieChart1[2].low,
                    //            drilldown: 'sitelow',
                    //            color: '#b8b7b7'
                    //        }]
                    //    }],
                    //    drilldown: {
                    //        series: [{
                    //            id: 'sitehigh',
                    //            data: [
                    //                { name: $scope.GetDulangPieChart2[0].incident_type, y: $scope.GetDulangPieChart2[0].percentage },
                    //                { name: $scope.GetDulangPieChart2[1].incident_type, y: $scope.GetDulangPieChart2[0].percentage },
                    //                { name: $scope.GetDulangPieChart2[2].incident_type, y: $scope.GetDulangPieChart2[0].percentage }
                    //            ],
                    //        }, {
                    //            id: 'sitemedium',
                    //            data: [
                    //                { name: $scope.GetDulangPieChart2[0].incident_type, y: $scope.GetDulangPieChart2[0].percentage },
                    //                { name: $scope.GetDulangPieChart2[1].incident_type, y: $scope.GetDulangPieChart2[0].percentage },
                    //                { name: $scope.GetDulangPieChart2[2].incident_type, y: $scope.GetDulangPieChart2[0].percentage }
                    //            ],
                    //        }, {
                    //            id: 'sitelow',
                    //            data: [
                    //                 { name: $scope.GetDulangPieChart2[0].incident_type, y: $scope.GetDulangPieChart2[0].percentage },
                    //                { name: $scope.GetDulangPieChart2[1].incident_type, y: $scope.GetDulangPieChart2[0].percentage },
                    //                { name: $scope.GetDulangPieChart2[2].incident_type, y: $scope.GetDulangPieChart2[0].percentage }
                    //            ]
                    //        }]
                    //    }
                    //})

                })
                req.error(function (err, msg) {

                });
            })
            req.error(function (err, msg) {

            });


        }
        $scope.GetDulangPieChart();

        $scope.pitext = 'All';
        $scope.incidenttype = "All";
        $scope.filterByIncident = function (val) {
            $scope.pitext = val;
            $scope.incidenttype = val;
            $scope.GetChordChart();
            $scope.getElementScore();
            $scope.showDropScoreDetail = false;
            //   $scope.getDulangValue();
            $scope.getLessonLearntCollections();
        }

        $scope.dropDownText = 'high';
        $scope.filterByCorrelation = function (val) {
            $scope.dropDownText = val;
            $scope.GetChordChart();
            $scope.getElementScore();
            $scope.showDropScoreDetail = false;
            //  $scope.getDulangValue();
            $scope.getLessonLearntCollections();
        }

        $scope.topCatText = '10';
        $scope.filterByTopList = function (val) {
            $scope.topCatText = val;
            $scope.getElementScore();
            $scope.GetChordChart();
        }

      



        $scope.GetChordChart = function () {

            $('#Chord_chart svg').remove();
            var url = appLinkFactory.getUrl('getChord') + "?fromDateTime=" + "2014-07-01" + "&toDateTime=" + "2018-07-31" + "&correlation=" + $scope.dropDownText + "&limit=" + $scope.topCatText;

            if ($scope.pitext != 'All') {
                url += "&incident_type=" + $scope.pitext;
            }
            if ($scope.myText1 != '') {
                url += "&site=" + $scope.myText1;
            }

            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {
                $scope.chordCollection = data;
                var key = [];
                //key.push($scope.pitext);
                var keyind = {};
                var limit = 100;
                //keyind[$scope.pitext] = 0;

                data.forEach(function (e, i) {
                    if (key.indexOf(e.name) < 0 && i < limit) {
                        key.push(e.name);
                        keyind[e.name] = key.length - 1;
                    }

                    if (e.children && i < limit) {
                        e.children.forEach(function (f, j) {
                            if (key.indexOf(f.key) < 0) {
                                key.push(f.key);
                                keyind[f.key] = key.length - 1;
                            }
                        });
                    }
                });
                var matrix = new Array(key.length).fill(0);
                for (i = 0; i < key.length; i++) {
                    matrix[i] = new Array(key.length).fill(0);
                }
                $scope.chrodElementPicker = function (data1, data2) {
                    $scope.source = data1;
                    $scope.target = data2;

                    var sourceKey = key;

                    svg = d3.select("#Chord_chart svg")
                    svg.selectAll("path.chord").filter(function (d) {
                        return d.source.index != 2  && d.target.index != 4;
                    }).transition()
                      .style("opacity", 0)


                }
                data.forEach(function (e, i) {
                    // matrix[0][keyind[e.name]] = e.size;
                    if (i < limit) {
                        e.children.forEach(function (f, j) {
                            matrix[keyind[e.name]][keyind[f.key]] = f.value;
                        })
                    }
                })
                //console.log(key);
                //console.log(matrix);
                var outerRadius = 630 / 2,
                innerRadius = outerRadius - 130;
                $('#Chord_chart svg').remove();
                var fill = d3.scale.category20c();

                var chord = d3.layout.chord()
                    .padding(.04)
                    .sortSubgroups(d3.descending)
                    .sortChords(d3.descending);

                var arc = d3.svg.arc()
                    .innerRadius(innerRadius)
                    .outerRadius(innerRadius + 20);

                var svg = d3.select("#Chord_chart").append("svg")
                    .attr("width", outerRadius * 2)
                    .attr("height", outerRadius * 2)
                     .append("g")
                    .attr("transform", "translate(" + outerRadius + "," + outerRadius + ")");

                var indexByName = d3.map(),
                    nameByIndex = d3.map(),
                    n = 0;

                // Returns the Flare package name for the given class name.
                function name(name) {
                    return name.substring(0, name.lastIndexOf(".")).substring(6);
                }


                var mname = ['Fire', 'Welding', 'Lifting', 'Hose', 'Water', 'Pipe', 'Tubing', 'Wellhead', 'Stairs', 'Plug', 'Employee'];
                //matrix = [
                //[0, 3, 8, 05, 0, 0, 94, 0, 0, 64, 95],
                //[0, 7, 98, 05, 0, 65, 94, 0, 0, 64, 95],
                //[0, 0, 0, 05, 0, 65, 94, 0, 0, 0, 0],
                //[0, 1, 0, 1, 0, 0, 0, 0, 41, 32, 41],
                //[0, 0, 0, 45, 15, 37, 0, 0, 7, 6, 0],
                //[0, 0, 0, 1, 0, 91, 91, 45, 0, 3, 0],
                //[0, 12, 6, 0, 62, 0, 0, 0, 70, 40, 0],
                //[0, 0, 0, 5, 0, 0, 21, 67, 0, 32, 0],
                //[0, 0, 0, 0, 91, 0, 0, 6, 21, 31, 0],
                //[0, 0, 0, 0, 0, 0, 91, 9, 0, 0, 0],
                //[0, 0, 0, 0, 0, 0, 31, 0, 4, 0, 0],
                //];
                chord.matrix(matrix);

                var g = svg.selectAll(".group")
                    .data(chord.groups)
                  .enter().append("g")
                    .attr("class", "group");

                g.append("path")
                    .on("mouseover", mfade(0))
                    .on("mouseout", mfade(1))
                    .style("fill", function (d) { return fill(d.index); })
                    .style("stroke", function (d) { return fill(d.index); })
                    .attr("d", arc);
                g.append("text")
                     .each(function (d) { d.angle = (d.startAngle + d.endAngle) / 2; })
                     .attr("dy", ".35em")
                     .attr("transform", function (d) {
                         return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
                             + "translate(" + (innerRadius + 26) + ")"
                             + (d.angle > Math.PI ? "rotate(180)" : "");
                     })
                     .style("text-anchor", function (d) { return d.angle > Math.PI ? "end" : null; })
                     .text(function (d) { return key[d.index]; });  //'nameByIndex.get(d.index)'

                //console.table(matrix);
                //console.log(indexByName);
                //console.log(JSON.stringify(chord.chords()));

                svg.selectAll(".chord")
                    .data(chord.chords)
                  .enter().append("path")
                    .attr("class", "chord")
                    .style("stroke", function (d) { return d3.rgb(fill(d.source.index)).darker(); })
                    .style("fill", function (d) { return fill(d.source.index); })
                    .attr("d", d3.svg.chord().radius(innerRadius));



                d3.select(self.frameElement).style("height", outerRadius * 2 + "px");

                // Returns an event handler for fading a given chord group.
                function mfade(opacity) {

                    return function (g, i) {
                        svg.selectAll("path.chord")
                            .filter(function (d) {
                                return d.source.index != i && d.target.index != i;
                            })
                            .transition()
                            .style("opacity", opacity);
                    };
                }


               

            });
            req.error(function (err, msg) {

            })
        }

        $scope.GetChordChart();



        $scope.getElementScore = function () {
            var url = appLinkFactory.getUrl('getElementTable') + "?fromDateTime=" + "2014-07-01" + "&toDateTime=" + "2018-07-31" + "&correlation=" + $scope.dropDownText + "&limit=" + $scope.topCatText;

            if ($scope.pitext != 'All') {
                url += "&incident_type=" + $scope.pitext;

            }
            if ($scope.myText1 != '') {
                url += "&site=" + $scope.myText1;
            }
            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {
                $scope.getElementScoreData = data;
            })
            req.error(function (err, msg) {

            })
        }
        $scope.getElementScore();

        $scope.showDropScoreDetail = false;
        $scope.scoreDataDrop = function (val) {
            $scope.showDropScoreDetail = true;
            $scope.elementText = val;
            $scope.correlationTable();
            $scope.getLessonLearntCollections();
            $scope.getDulangValue();
            $scope.dulangChart1 = true;
            $scope.dulangChart2 = false;
            setTimeout(function () {
                $('body .scroll-pos').scrollTop(6000);
            }, 200)
        }


        $scope.getDulangValue = function () {
            var url = appLinkFactory.getUrl('getDulangValue') + "?fromDateTime=" + "2015-07-01" + "&toDateTime=" + "2017-07-05" + "&correlation=" + $scope.dropDownText + "&element=" + $scope.elementText;
            //+ "water"
            if ($scope.pitext != 'All') {
                url += "&incident_type=" + $scope.pitext;
            }
            if ($scope.myText1 != '') {
                url += "&site=" + $scope.myText1;
            }
            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {
                $scope.getDulangNumbers = data;
                // Create the chart
                //Highcharts.chart('tree-graph', {
                //    chart: {
                //        type: 'treemap',
                //    },
                //    title: {
                //        text: ''
                //    },
                //    subtitle: {
                //        text: ''
                //    },
                //    exporting: { enabled: false },
                //    credits: { enabled: false },
                //    plotOptions: {
                //        series: {
                //            dataLabels: {
                //                enabled: true,
                //                color: '#fff',
                //                format: 'DL:{point.value}',
                //                style: {
                //                    textOutline: false,
                //                    textShadow: false,
                //                }

                //            }
                //        }
                //    },

                //    tooltip: {
                //        enabled: true
                //    },
                //    series: [{
                //        name: 'Reported By Site',
                //        colorByPoint: true,
                //        data: [
                //         {
                //            name: $scope.getDulangNumbers[0].site,
                //            value: $scope.getDulangNumbers[0].count,
                //            color: '#76b4f2'

                //        }, {
                //            name: $scope.getDulangNumbers[1] == undefined ? '' : $scope.getDulangNumbers[1].site,
                //            value: $scope.getDulangNumbers[1] == undefined ? 0 : $scope.getDulangNumbers[1].count,
                //            color: '#f27676'
                //        }, {
                //            name: $scope.getDulangNumbers[2] == undefined ? '' : $scope.getDulangNumbers[2].site,
                //            value: $scope.getDulangNumbers[2] == undefined ? 0 : $scope.getDulangNumbers[2].count,
                //            color: '#f2c476'
                //        }, {
                //            name: $scope.getDulangNumbers[3] == undefined ? '' : $scope.getDulangNumbers[3].site,
                //            value: $scope.getDulangNumbers[3] == undefined ? 0 : $scope.getDulangNumbers[3].count,
                //            color: '#333333'
                //        }],
                //    }],
                //});


                Highcharts.chart('tree-graph', {
                    series: [{
                        type: "treemap",
                        layoutAlgorithm: 'squarified',
                        allowDrillToNode: false,
                        
                        data: [{
                                        name: $scope.getDulangNumbers[0].site,
                                        value: $scope.getDulangNumbers[0].count,
                                        color: '#76b4f2'

                                    }, {
                                        name: $scope.getDulangNumbers[1] == undefined ? '' : $scope.getDulangNumbers[1].site,
                                        value: $scope.getDulangNumbers[1] == undefined ? 0 : $scope.getDulangNumbers[1].count,
                                        color: '#f27676'
                                    }, {
                                        name: $scope.getDulangNumbers[2] == undefined ? '' : $scope.getDulangNumbers[2].site,
                                        value: $scope.getDulangNumbers[2] == undefined ? 0 : $scope.getDulangNumbers[2].count,
                                        color: '#f2c476'
                                    }, {
                                        name: $scope.getDulangNumbers[3] == undefined ? '' : $scope.getDulangNumbers[3].site,
                                        value: $scope.getDulangNumbers[3] == undefined ? 0 : $scope.getDulangNumbers[3].count,
                                        color: '#333333'
                                  }]
                    }],
                        exporting: { enabled: false },
                            credits: { enabled: false },
                            plotoptions: {
                                series: {
                                    datalabels: {
                                        enabled: true,
                                        color: '#fff',
                                        format: 'dl:{point.value}',
                                        style: {
                                            textoutline: false,
                                            textshadow: false,
                                        }

                                    }
                                }
                            },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    }
                });
            })
            req.error(function (err, msg) {

            })
        }
        $scope.getDulangValue();

        $scope.dulangChart1 = true;
        $scope.dulangChartTab1 = function () {
            $scope.dulangChart1 = true;
            $scope.dulangChart2 = false;
        }

        $scope.dulangChart2 = false;
        $scope.dulangChartTab2 = function () {
            $scope.dulangChart2 = true;
            $scope.dulangChart1 = false;
        }

        $scope.getLessonLearntCollections = function () {
            var url = appLinkFactory.getUrl('getLessonLearntValue') + "?fromDateTime=" + "2016-07-01" + "&toDateTime=" + "2017-07-05" + "&correlation=" + $scope.dropDownText + "&element=" + $scope.elementText;
            if ($scope.pitext != 'All') {
                url += "&incident_type=" + $scope.pitext;
            }
            if ($scope.myText1 != '') {
                url += "&site=" + $scope.myText1;
            }
            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {
                $scope.getLessonLearntData = data;
            })
            req.error(function (err, msg) {

            })
        }
        $scope.getLessonLearntCollections();


        $scope.getProbableIncidents = function () {
            var url = appLinkFactory.getUrl('GetIncidentWiseCorrelation') + "?fromDateTime=" + "2017-07-01" + "&toDateTime=" + "2017-07-31";
            if ($scope.myText1 != '') {
                url += "&site=" + $scope.myText1;
            }
            var req = httpRequestService.connect(url, 'GET');
            req.success(function (data) {
                $scope.getProbableIncidents = data;

                // Dimensions of sunburst.
                var width = 300;
                var height = 300;
                var radius = Math.min(width, height) / 2;

                // Breadcrumb dimensions: width, height, spacing, width of tip/tail.
                var b = {
                    w: 75, h: 30, s: 3, t: 10
                };

                // make `colors` an ordinal scale
                var colors = d3.scale.ordinal()
                .range(["#FEC95A", "#FAA74F", "#c964a0", "#5ec9c8", "#dcc9ae", "#EF7E45"]);


                // Total size of all segments; we set this later, after loading the data.
                var totalSize = 0;

                var vis = d3.select("#chart").append("svg:svg")
                    .attr("width", width)
                    .attr("height", height)
                    .append("svg:g")
                    .attr("id", "chart")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                var partition = d3.layout.partition()
                    .size([2 * Math.PI, radius * radius])
                    .value(function (d) { return d.count; });

                var arc = d3.svg.arc()
                    .startAngle(function (d) { return d.x; })
                    .endAngle(function (d) { return d.x + d.dx; })
                    .innerRadius(function (d) { return Math.sqrt(d.y); })
                    .outerRadius(function (d) { return Math.sqrt(d.y + d.dy); });

                // Use d3.csv.parseRows so that we do not need to have a header
                // row, and can receive the csv as an array of arrays.

                //var text = getText();
                //var csv = d3.csv.parseRows(text);
                //var json = buildHierarchy(csv);
                var json = getData();
                createVisualization(json);




                // Main function to draw and set up the visualization, once we have the data.
                function createVisualization(json) {

                    // Basic setup of page elements.
                    initializeBreadcrumbTrail();

                    d3.select("#togglelegend").on("click", toggleLegend);


                    // Bounding circle underneath the sunburst, to make it easier to detect
                    // when the mouse leaves the parent g.
                    vis.append("svg:circle")
                        .attr("r", 50)
                        .style("opacity", 0);

                    // For efficiency, filter nodes to keep only those large enough to see.
                    var nodes = partition.nodes(json)
                        .filter(function (d) {
                            return (d.dx > 0.005); // 0.005 radians = 0.29 degrees
                        });

                    var uniqueNames = (function (a) {
                        var output = [];
                        a.forEach(function (d) {
                            if (output.indexOf(d.name) === -1) {
                                output.push(d.name);
                            }
                        });
                        return output;
                    })(nodes);

                    // set domain of colors scale based on data
                    colors.domain(uniqueNames);

                    // make sure this is done after setting the domain
                    drawLegend();


                    var path = vis.data([json]).selectAll("path")
                        .data(nodes)
                        .enter().append("svg:path")
                        .attr("display", function (d) { return d.depth ? null : "none"; })
                        .attr("d", arc)
                        .attr("fill-rule", "evenodd")
                        .style("fill", function (d) { return colors(d.name); })
                        .style("opacity", 1)
                        .on("mouseover", mouseover);

                    // Add the mouseleave handler to the bounding circle.
                    d3.select("#chart").on("mouseleave", mouseleave);

                    // Get total size of the tree = value of root node from partition.
                    totalSize = path.node().__data__.value;
                };

                // Fade all but the current sequence, and show it in the breadcrumb trail.
                function mouseover(d) {

                    var percentage = (100 * d.value / totalSize).toPrecision(3);
                    var percentageString = percentage + "%";
                    if (percentage < 0.1) {
                        percentageString = "< 0.1%";
                    }
                    if (d.depth > 1) {
                        //Condition for showing inner value of FIRE, LOPC, PI
                        //percentageString = d.value + " %";
                        var t = 0;
                        d.parent.children.forEach(function (e, i) {
                            t += e.count;
                        });
                        percentageString = parseFloat(d.value / t * 100).toFixed(2) + "%";
                        
                    }

                    d3.select("#percentage")
                        .text(percentageString);

                    d3.select("#explanation")
                          .transition()
                          .duration(1000)
                          .style("visibility", "visible");

                    var sequenceArray = getAncestors(d);

                    updateBreadcrumbs(sequenceArray, percentageString);

                    // Fade all the segments.
                    d3.selectAll("path")
                        .style("opacity", 0.3);

                    // Then highlight only those that are an ancestor of the current segment.
                    vis.selectAll("path")
                        .filter(function (node) {
                            return (sequenceArray.indexOf(node) >= 0);
                        })
                        .style("opacity", 1);
                }

                // Restore everything to full opacity when moving off the visualization.
                function mouseleave(d) {

                    // Hide the breadcrumb trail
                    d3.select("#trail")
                        .style("visibility", "hidden");

                    // Deactivate all segments during transition.
                    d3.selectAll("path").on("mouseover", null);

                    // Transition each segment to full opacity and then reactivate it.
                    d3.selectAll("path")
                        .transition()
                        .duration(1000)
                        .style("opacity", 1)
                        .each("end", function () {
                            d3.select(this).on("mouseover", mouseover);
                        });

                    d3.select("#explanation")
                        .transition()
                        .duration(1000)
                        .style("visibility", "hidden");
                }

                // Given a node in a partition layout, return an array of all of its ancestor
                // nodes, highest first, but excluding the root.
                function getAncestors(node) {
                    var path = [];
                    var current = node;
                    while (current.parent) {
                        path.unshift(current);
                        current = current.parent;
                    }
                    return path;
                }

                function initializeBreadcrumbTrail() {
                    // Add the svg area.
                    var trail = d3.select("#sequence").append("svg:svg")
                        .attr("width", width)
                        .attr("height", 50)
                        .attr("id", "trail");
                    // Add the label at the end, for the percentage.
                    trail.append("svg:text")
                      .attr("id", "endlabel")
                      .style("fill", "#000");
                }

                // Generate a string that describes the points of a breadcrumb polygon.
                function breadcrumbPoints(d, i) {
                    var points = [];
                    points.push("0,0");
                    points.push(b.w + ",0");
                    points.push(b.w + b.t + "," + (b.h / 2));
                    points.push(b.w + "," + b.h);
                    points.push("0," + b.h);
                    if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
                        points.push(b.t + "," + (b.h / 2));
                    }
                    return points.join(" ");
                }

                // Update the breadcrumb trail to show the current sequence and percentage.
                function updateBreadcrumbs(nodeArray, percentageString) {

                    // Data join; key function combines name and depth (= position in sequence).
                    var g = d3.select("#trail")
                        .selectAll("g")
                        .data(nodeArray, function (d) { return d.name + d.depth; });

                    // Add breadcrumb and label for entering nodes.
                    var entering = g.enter().append("svg:g");

                    entering.append("svg:polygon")
                        .attr("points", breadcrumbPoints)
                        .style("fill", function (d) { return colors(d.name); });

                    entering.append("svg:text")
                        .attr("x", (b.w + b.t) / 2)
                        .attr("y", b.h / 2)
                        .attr("dy", "0.35em")
                        .attr("text-anchor", "middle")
                        .text(function (d) { return d.name; });

                    // Set position for entering and updating nodes.
                    g.attr("transform", function (d, i) {
                        return "translate(" + i * (b.w + b.s) + ", 0)";
                    });

                    // Remove exiting nodes.
                    g.exit().remove();

                    // Now move and update the percentage at the end.
                    d3.select("#trail").select("#endlabel")
                        .attr("x", (nodeArray.length + 0.5) * (b.w + b.s))
                        .attr("y", b.h / 2)
                        .attr("dy", "0.35em")
                        .attr("text-anchor", "middle")
                        .text(percentageString);

                    // Make the breadcrumb trail visible, if it's hidden.
                    d3.select("#trail")
                        .style("visibility", "");

                }

                function drawLegend() {

                    // Dimensions of legend item: width, height, spacing, radius of rounded rect.
                    var li = {
                        w: 75, h: 30, s: 3, r: 3
                    };

                    var legend = d3.select("#legend").append("svg:svg")
                        .attr("width", li.w)
                        .attr("height", colors.domain().length * (li.h + li.s));

                    var g = legend.selectAll("g")
                        .data(colors.domain())
                        .enter().append("svg:g")
                        .attr("transform", function (d, i) {
                            return "translate(0," + i * (li.h + li.s) + ")";
                        });

                    g.append("svg:rect")
                        .attr("rx", li.r)
                        .attr("ry", li.r)
                        .attr("width", li.w)
                        .attr("height", li.h)
                        .style("fill", function (d) { return colors(d); });

                    g.append("svg:text")
                        .attr("x", li.w / 2)
                        .attr("y", li.h / 2)
                        .attr("dy", "0.35em")
                        .attr("text-anchor", "middle")
                        .text(function (d) { return d; });
                }

                function toggleLegend() {
                    var legend = d3.select("#legend");
                    if (legend.style("visibility") == "hidden") {
                        legend.style("visibility", "");
                    } else {
                        legend.style("visibility", "hidden");
                    }
                }



                function getData() {
                    return {
                        "name": "PI",
                        "children": $scope.getProbableIncidents
                    };
                };



            })
            req.error(function (err, msg) {

            })
        }
      
        $scope.myText1 = '';
        $scope.getDulangName = function (val) {
            $scope.myText1 = val;
            //$scope.gettableName('', 'activity');
           $scope.getElementScore();
           // $scope.GetDulangUAUCCatPercentage(myText1);
           // $scope.filterByIncident(myText1);
            $scope.GetChordChart(val);
            $scope.correlationTable();
            $scope.getDulangValue();
            $scope.getLessonLearntCollections();


           // $scope.getProbableIncidents();
        }



        $scope.getProbableIncidents();

   




    }]);